FROM debian:bullseye as build

ARG PREFLIGHT_VERSION=1.2.1
ARG PREFLIGHT_URL=https://github.com/redhat-openshift-ecosystem/openshift-preflight/releases/download/${PREFLIGHT_VERSION}/preflight-linux-amd64
ENV PREFLIGHT_VERSION=${PREFLIGHT_VERSION}

RUN apt update -y
RUN apt install -y curl
RUN curl -fsSL -o /usr/local/bin/preflight $PREFLIGHT_URL
RUN chmod 555 /usr/local/bin/preflight

FROM debian:bullseye-slim
COPY --from=build /usr/local/bin/preflight /usr/local/bin/preflight
RUN apt update -y && apt install -y podman ca-certificates jq && \
    rm -rf /var/lib/apt/lists/* && \
    echo 'event_logger = "file"' > /etc/containers/containers.conf
