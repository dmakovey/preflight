preflight container
===================

This is a packaging of the Red Hat preflight binary for certification pipelines.

GitHub repo where preflight source is located: https://github.com/redhat-openshift-ecosystem/openshift-preflight

Typical usage
-------------

```shell
❯ docker run -it --rm preflight:1.1.0 preflight check container registry.gitlab.com/gitlab-org/build/cng/gitlab-shell:2846-retool-certification-ubi8
time="2022-04-23T00:07:39Z" level=info msg="certification library version 1.1.0 <commit: f1628fcf489aed88711d6cac95c3f096217f9feb>"
time="2022-04-23T00:08:45Z" level=info msg="check completed: LayerCountAcceptable" result=PASSED
time="2022-04-23T00:08:45Z" level=info msg="check completed: HasNoProhibitedPackages" result=PASSED
time="2022-04-23T00:08:45Z" level=info msg="check completed: HasRequiredLabel" result=PASSED
time="2022-04-23T00:08:45Z" level=info msg="USER git:git specified that is non-root"
time="2022-04-23T00:08:45Z" level=info msg="check completed: RunAsNonRoot" result=PASSED
time="2022-04-23T00:08:46Z" level=info msg="check completed: BasedOnUbi" result=PASSED
time="2022-04-23T00:08:58Z" level=info msg="check completed: HasModifiedFiles" result=PASSED
time="2022-04-23T00:08:58Z" level=info msg="check completed: HasLicense" result=PASSED
time="2022-04-23T00:08:59Z" level=info msg="check completed: HasUniqueTag" result=PASSED
{
    "image": "registry.gitlab.com/gitlab-org/build/cng/gitlab-shell:2846-retool-certification-ubi8",
    "passed": true,
    "test_library": {
        "name": "github.com/redhat-openshift-ecosystem/openshift-preflight",
        "version": "1.1.0",
        "commit": "f1628fcf489aed88711d6cac95c3f096217f9feb"
    },
    "results": {
        "passed": [
            {
                "name": "LayerCountAcceptable",
                "elapsed_time": 0,
                "description": "Checking if container has less than 40 layers.  Too many layers within the container images can degrade container performance."
            },
            {
                "name": "HasNoProhibitedPackages",
                "elapsed_time": 124,
                "description": "Checks to ensure that the image in use does not include prohibited packages, such as Red Hat Enterprise Linux (RHEL) kernel packages."
            },
            {
                "name": "HasRequiredLabel",
                "elapsed_time": 1,
                "description": "Checking if the required labels (name, vendor, version, release, summary, description) are present in the container metadata."
            },
            {
                "name": "RunAsNonRoot",
                "elapsed_time": 1,
                "description": "Checking if container runs as the root user because a container that does not specify a non-root user will fail the automatic certification, and will be subject to a manual review before the container can be approved for publication"
            },
            {
                "name": "BasedOnUbi",
                "elapsed_time": 912,
                "description": "Checking if the container's base image is based upon the Red Hat Universal Base Image (UBI)"
            },
            {
                "name": "HasModifiedFiles",
                "elapsed_time": 11939,
                "description": "Checks that no files installed via RPM in the base Red Hat layer have been modified"
            },
            {
                "name": "HasLicense",
                "elapsed_time": 0,
                "description": "Checking if terms and conditions applicable to the software including open source licensing information are present. The license must be at /licenses"
            },
            {
                "name": "HasUniqueTag",
                "elapsed_time": 947,
                "description": "Checking if container has a tag other than 'latest', so that the image can be uniquely identified."
            }
        ],
        "failed": [],
        "errors": []
    }
}
time="2022-04-23T00:09:00Z" level=info msg="Preflight result: PASSED"
```
